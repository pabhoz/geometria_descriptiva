/*var myBtn = document.createElement("button");
myBtn.id = "mybtn";
myBtn.innerHTML = "Mi Boton OP";
document.body.appendChild(myBtn);
myBtn.addEventListener('click', soyOP);

var myBtn2 = document.createElement("button");
myBtn2.id = "mybtn2";
myBtn2.innerHTML = "Mi Boton OP Recargado";
document.body.appendChild(myBtn2);
myBtn2.addEventListener('click', soyOP);

function soyOP(){
	alert("Soy Muy OP");
}*/
var consola = document.getElementById('console').getElementsByClassName('body')[0];

var addV3Btn = document.getElementById("addV3Btn");
addV3Btn.addEventListener('click', addVectors);

function addVectors(){

	var u = crearVector();
	var v = crearVector();
	console.log(u);
	console.log(v);
	var u_add_v = u.add(v);
	consola.innerHTML += "<p> >> Suma de vectores </p>";
	consola.innerHTML += "<p> >> Vector3 u:"+JSON.stringify ( u ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 v:"+JSON.stringify ( v ) + " </p>";
	consola.innerHTML += "<p> >> Vector3 u+v:"+JSON.stringify ( u_add_v ) + " </p>";
}

function crearVector(){

	var message = 'Digite las componentes del vector 3D separadas\
				   por comas \",\" o un escalar para asignar a las\
				   3 componentes';

	var value = prompt(message,"0,0,0");

	if( value != null){
		value = value.split(",");
		for (var i = 0; i < value.length; i++) {
			value[i] = parseFloat(value[i]);
		};

		if(value.length > 1){
			return new Vector3(value[0],value[1],value[2]);
		}else{
			return new Vector3(value[0]);
		}
	}
}